<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Books</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>

<style type="text/css">
body {
	background: gray;
}

.container {
	align-self: center;
	align-content: center;
}

.nav {
	background-color: rgb(56, 56, 56);
	top: 0;
	width: 100%;
	display: flex;
	justify-content: space-between;
	padding: 10px;
	z-index: 1;
	transition-timing-function: ease-in;
}

a {
	padding-right: 15px;
	color: white;
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
	color: white;
}
span {
	color: white;
	background-color: red;
}
</style>

</head>
<body>
	<div class="nav" id="nav" style="background-color: transparent;">
		<a href="books">Books</a>
		<div class="join-box">
			<p class="join-msg">
				<a href="readLaterbooks">Read Later</a> 
				<a href="likedbooks">Liked</a> <a
					href="login">Sign in</a>
			</p>
		</div>
	</div>
	<div class='container'>
		<h4 class='login purple'>Login</h4>

		<form action="login" method="post">
			<%
			String error = (String)session.getAttribute("error");
			if(error!=null){
			%>
			<span><%=error %></span>
			<%} %>
			<div class="row">
				<div class="col-lg-6 col-lg-offset-3">
					<div class="form-group">
						<label for="username">Email:</label> <input type="text"
							class="form-control" id="email" placeholder="Enter email"
							name="email">
					</div>
					<div class="form-group">
						<label for="password">Password:</label> <input type="password"
							class="form-control" id="email" placeholder="Enter Password"
							name="password">
					</div>

					<div align="center">
						<button type="submit" class="btn btn-primary" value="Login">Login</button>
					</div>
				</div>
			</div>
		</form>
		<div>
			<p>New to Books</p>
			<button type="button" class="btn btn-danger">
				<a href='register'>Sign up</a>
			</button>
		</div>
	</div>
</body>
</html>