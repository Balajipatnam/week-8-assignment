package com.springmvc.assignment.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.springmvc.assignment.model.Book;

@Repository
public class BookDatabase {
	
	@Autowired
	private JdbcTemplate template;
	@Autowired
	private UserDatabase udb;

	public List<Book> getAllBooks() throws SQLException {
		
		String sql = "select  * from book";
		
		System.out.println(this.template);
		return  this.template.query(sql,(ResultSet rs, int arg1)->{
				Book book = new Book();
				book.setBookId(rs.getInt(1));
				book.setName(rs.getString(2));
				book.setAuthor_name(rs.getString(3));
				book.setType(rs.getString(4));
				book.setPageCount(rs.getInt(5));
				book.setPoster(rs.getString(6));
				return book;		
		});
	}
	
	public List<Book> getlikedBooks(String email){
		int userId = 0;
		try {
			userId = udb.getUserId(email);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String sql = "select distinct book.bookId, name, author, type, pagecount, poster from book,likedbooks where book.bookId=likedbooks.bookId"
				+ " and likedbooks.userId="+userId;
		return this.template.query(sql,(ResultSet rs, int arg1)->{
			Book book = new Book();
			book.setBookId(rs.getInt(1));
			book.setName(rs.getString(2));
			book.setAuthor_name(rs.getString(3));
			book.setType(rs.getString(4));
			book.setPageCount(rs.getInt(5));
			book.setPoster(rs.getString(6));
			return book;
		});
	}
	
	public List<Book> getReadLaterBooks(String email){
		int userId = 0;
		try {
			userId = udb.getUserId(email);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String sql = "select distinct book.bookId, name, author, type, pagecount, poster from book,readlaterbooks,user "
				+ "where book.bookId=readlaterbooks.bookId and readlaterbooks.userId="+userId;
		return this.template.query(sql,(ResultSet rs, int arg1)->{
			Book book = new Book();
			book.setBookId(rs.getInt(1));
			book.setName(rs.getString(2));
			book.setAuthor_name(rs.getString(3));
			book.setType(rs.getString(4));
			book.setPageCount(rs.getInt(5));
			book.setPoster(rs.getString(6));
			return book;
		});
	}
	
	public boolean addFavBook(int bookId,String email) {
		int userId = 0;
		try {
			userId = udb.getUserId(email);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String sql = "insert into likedbooks(bookId,userId) value(?,?)";
		this.template.update(sql,bookId,userId);
		return true;
	}
	
	public boolean addReadlaterBook(int bookId,String email) {
		int userId = 0;
		try {
			userId = udb.getUserId(email);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String sql = "insert into readlaterbooks(bookId,userId) value(?,?)";
		this.template.update(sql,bookId,userId);
		return true;
	}
	
	public boolean removefavbook(int bookId,String email) throws SQLException {
		String sql = "delete from likedbooks where bookId=? and userId=?";
		int userId = udb.getUserId(email);
		this.template.update(sql,bookId,userId);
		return true;
	}
	
	public boolean removeReadlaterBook(int bookId,String email) throws SQLException {
		String sql = "delete from readlaterbooks where bookId=? and userId=?";
		int userId = udb.getUserId(email);
		this.template.update(sql,bookId,userId);
		return true;
	}
}
