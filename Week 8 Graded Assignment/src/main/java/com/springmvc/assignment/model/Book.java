package com.springmvc.assignment.model;

import org.springframework.stereotype.Component;

@Component
public class Book {

	private int bookId;
	private String name;
	private String author_name;
	private String type;
	private int pageCount;
	private String poster;

	public Book() {
	}
	
	public Book(int bookId, String name, String author_name, String author_surname, String type, int pagecount, String poster) {
		super();
		this.bookId = bookId;
		this.name = name;
		this.author_name = author_name;
		this.type = type;
		this.pageCount = pagecount;
		this.poster = poster;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor_name() {
		return author_name;
	}

	public void setAuthor_name(String author_name) {
		this.author_name = author_name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pagecount) {
		this.pageCount = pagecount;
	}
	
	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", name=" + name + ", author_name=" + author_name +", type=" + type + ", pagecount=" + pageCount 
				+ " poster: " +poster+"]";
	}
	
	
	
}
