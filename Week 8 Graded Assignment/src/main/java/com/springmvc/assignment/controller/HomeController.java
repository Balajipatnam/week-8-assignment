package com.springmvc.assignment.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.springmvc.assignment.database.BookDatabase;
import com.springmvc.assignment.database.UserDatabase;
import com.springmvc.assignment.model.Book;
import com.springmvc.assignment.model.LoginUser;
import com.springmvc.assignment.model.User;

@Controller
public class HomeController {

	public HomeController() {
		System.out.println("home controller");
	}

	@Autowired
	private UserDatabase userDb;
	@Autowired
	private BookDatabase bookDB;

	@GetMapping("/login")
	public String loginPage(HttpSession session) {
		System.out.println("login get called");
		session.removeAttribute("error");
		return "login";
	}

	@PostMapping("/login")
	public String loginUser(LoginUser loginUser, Map<String, String> map ,HttpSession session) {

		if(userDb.validate(loginUser)) {
			session.setAttribute("email", loginUser.getEmail());
			List<Book> books = null;
			try {
				books = bookDB.getAllBooks();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			session.setAttribute("books", books);
			session.removeAttribute("error");
			map.put("email", loginUser.getEmail());
			return "books";
		}
		session.setAttribute("error", "Invalid Credentials");
		return "login";
	}

	@GetMapping("/register")
	public String register() {
		return "register";
	}

	@PostMapping("/register")
	public String registerUser(User user, Map<String, String> map) {

		try {
			if(userDb.insertUser(user))
				return "login";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "books";
	}
	
	@GetMapping("/logout")
	public String logout(HttpServletRequest request,HttpServletResponse response) {
		Cookie cookies[] = request.getCookies();
		for(Cookie cookie : cookies)
		{
			if(cookie.getName().equals("JSESSIONID"))
				{
				cookie.setMaxAge(0);
				response.addCookie(cookie);
				}
		}
		HttpSession session = request.getSession();
		session.removeAttribute("email");
		session.invalidate();
		return "login";
	}
}
